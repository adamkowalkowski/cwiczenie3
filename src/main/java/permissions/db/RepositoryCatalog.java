package permissions.db;

public interface RepositoryCatalog {
	
	public PersonRepository people();
	public AddressRepository address();
	public PermissionRepository permission();
	public RoleRepository role();
	public UserRepository user();
}
